function [ output_args ] = CstegerWhiskerDetect( rx, ry, a, b, d, iy, ix, tr2)
%CSTEGERWHISKERDETECT Steger curvilinear structure detection
%   This function implements the curvilinear structure detection. It has
%   rx, ry, and the components of the Hessian matrix [a b; b d] as imput,
%   as well as the position of the pixel in the image, and the upper limit
%   for the largest eigenvector.
%   Eigenvalue and eigenvector computation is implemented using only
%   scalars, to make it possible to run this function on a GPU.


%% SERIAL CALCULATION OF EIGENVALUES
T = a+d;                                                                    % Trace
D = a*d-b.^2;                                                               % determinant
egv1 = T/2+power(complex((T^2/4-D)),complex(0.5));                          % first eigenvalue (with protection in case it is complex)          
egv2 = T/2-power(complex((T^2/4-D)),complex(0.5));                          % second eigenvalue
if abs(egv1) > abs(egv2)                                                    % find largest eigenvalue
    egv = real(egv1);
else
    egv = real(egv2);
end
ev1 = egv - d;                                                              % first component of eigenvector. Second one is equal to b
n = sqrt(ev1^2+b^2);                                                        % length of eigenvector (necessary for normalization)
ev1 = ev1/n;                                                                % normalize eigenvector
b = b/n;


t = -(rx*ev1+ry*b)/ (a*ev1^2+2*b*ev1*b+d*b^2);                              % calculate t
xComponent_t = t*ev1;
yComponent_t = t*b;                                                         %  X and Y components of t.
hereIsAWhisker = (abs(xComponent_t) < 0.5) & ...
    (abs(yComponent_t) < 0.5) & (egv < tr2);                                % detect presence of a whisker
if hereIsAWhisker                                                           % if there is a whisker, return xposition + yposition*i
    output_args = ix+xComponent_t + ...
        complex(iy+yComponent_t)*1i;
else
    output_args = complex(-1);                                              % else, return -1 + 0i
end

end

