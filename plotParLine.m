function [ xyRotated ] = plotParLine( theLine )
%PLOTPARLINE This function is just used for plotting a line in [xf a b L]
% notation.
%   Input the line, and it will return a series of coordinates which can be
%   plotted. If the length of the input is incorrect, it will return an
%   empty array.
if length(theLine) == 4 
    x = 0:theLine(4);
    y = theLine(3)*x.^2;
    q = theLine(2);
    xyRotated = [[cos(q) -sin(q); sin(q) cos(q)]*[x;y]]'+[theLine(1) 0];
else
    xyRotated = [];
end
end

