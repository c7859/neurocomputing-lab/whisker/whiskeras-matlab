function [ output_args ] = remove_edge( theImage, t )
%REMOVE_EDGE Remove the edge with thickness t of an image, replace it by
%zeros. Use: give the image itself and the desired edge thickness, it will
%return the image with a frame of zeros.
xyFig = size(theImage);                                                     % The code is straightforward
thFrame = ones(xyFig);
for a = 1:xyFig(1)
    for b = 1:xyFig(2)
        if a <= t || b <= t || a >= xyFig(1)-t || b >= xyFig(2)-t
            thFrame(a,b) = 0;
        end
    end
end
output_args = thFrame.*theImage;

end

