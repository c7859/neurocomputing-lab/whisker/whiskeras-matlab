function [ order, missedClusters, missedWhiskers] = recognizer_Nexpert(lineGatherer_tracker, lineGatherer_detector, recog_op, N_expert_op, track_opt)
%BINARYDECISIONS This function implements both the recognizer (a set of
%SVMs, not including the training phase) and the N-expert.
%   Detailed explanation goes here

min_Xf = min(cellfun(@(x) x(1),lineGatherer_tracker));                      % Get the minimal and maximal values for all the
max_Xf = max(cellfun(@(x) x(1),lineGatherer_tracker));                      % parameters from the previous frame. This will help in normalization.
min_A = min(cellfun(@(x) x(2),lineGatherer_tracker));
max_A = max(cellfun(@(x) x(2),lineGatherer_tracker));
min_L = min(cellfun(@(x) x(4),lineGatherer_tracker));
max_L = max(cellfun(@(x) x(4),lineGatherer_tracker));
min_B = min(cellfun(@(x) x(3),lineGatherer_tracker));
max_B = max(cellfun(@(x) x(3),lineGatherer_tracker));

normXf = max_Xf-min_Xf;                                                     % Dividing the differences in parameters between the 
normA = max_A-min_A;                                                        % two frames by these values will normalize them,
normL = max_L-min_L;                                                        % which makes it possible to weight them properly
normB = max_B-min_B;

currentClusters = cell(size(lineGatherer_detector));                        % This will contain the data from the detected whiskers in this frame.                   

for i = 1:length(lineGatherer_detector)                                     % For every detected whisker in frame n
    currentClusters{i}.x1_p = lineGatherer_detector{i}(1);                  % Get the position and angle.
    currentClusters{i}.x2_p = lineGatherer_detector{i}(2);
    currentClusters{i}.x1_a = track_opt.prevXfAlpha(1);                          % Take the average of these values from the previous frame (since the current frame is not known yet)
    currentClusters{i}.x2_a = track_opt.prevXfAlpha(2);
    currentClusters{i}.L = lineGatherer_detector{i}(4);                     % Length
    currentClusters{i}.cnf = 0;
    currentClusters{i}.x3 = lineGatherer_detector{i}(3);                    % bending
end

choicePerCluster = zeros(size(lineGatherer_detector));                      % For every cluster: what is the most likely match from the whiskers from earlier frames?               

for k = 1:length(lineGatherer_detector)                                     % For every detected whisker
    currentCluster = prepareTrainingData(currentClusters{k},0,1,0);         % format data in the same format as the training data, so that the SVM can make sense of it
    before = 1:length(lineGatherer_tracker);                                % All the 'candidates' for recognition
    after = [1 1];
    while length(after) > 1                                                 % While the final choice hasn't been made yet
        option1 = before(1:2:end);                                          % Divide the candidates into two groups
        option2 = before(2:2:end);                                          % So that we can make pairs of candidates
        Lo1 = length(option1);
        Lo2 = length(option2);
        after = zeros(1,max(Lo1,Lo2));                                      % This array will contain all the candidates that are still left.
        for i = 1:min(Lo1,Lo2)                                              % For every pair of candidates: which one is more likely for this (unknown) whisker?
            after(i) = predict(recog_op.svm_matrix{option2(i), ...
                option1(i)},currentCluster);                                % Here, the SVM recognition happens
        end
        if Lo1 > Lo2                                                        % if the groups were not of equal size (when the number of candidates is odd), just add the `leftover' whisker to the pool of candidates and test it next iteration.
            after(end) = option1(end);
        elseif Lo1 < Lo2
            after(end) = option2(end);
        end
        before = after;
    end
    choicePerCluster(k) = after;                                            % When the choice is made, fill it in in this array
end

order = zeros(length(lineGatherer_tracker),1);                              % Prepare an array to fill in the final matching. Basically, what we want do, is convert the matching of new whiskers to old one, into a matching of new whiskers to old ones.
for i = 1:length(lineGatherer_tracker)
    a = find(choicePerCluster == i);                                        % How many options do we have per whisker?
    if length(a) == 1                                                       % if there's only one option, then that's the only choice, and we're done.
        order(i) = a;
    elseif length(a) > 1                                                    % What if more than one newly detected whisker are recognized as the same whisker?
        tempMat = cell2mat(lineGatherer_detector(a)'); 
        thisWhisker = [lineGatherer_tracker{i}(1) ...
            lineGatherer_tracker{i}(2) lineGatherer_tracker{i}(3) ...
            lineGatherer_tracker{i}(4)];                                    
        diffs = abs(thisWhisker-tempMat(:,1:4))./ ...                       % Then we choose the cluster that is most similar to the matched whisker.
            [normXf normA normB normL];
        [~,chosenCluster] = min(recog_op.weightX*diffs(:,1).^2+...
            recog_op.weightA*diffs(:,2).^2+ ...
            recog_op.weightB*diffs(:,3).^2 + recog_op.weightL*diffs(:,4).^2);
        order(i) = a(chosenCluster);
        a(chosenCluster) = [];
        choicePerCluster(a) = 0; 
    else
        disp(['Could not fit whisker ' num2str(i) '.']);                    % No newly detected whiskers recognized as this whisker?
    end
end

for i = 1:length(lineGatherer_tracker)                                      % N-expert
    if order(i)
        diffs = abs(lineGatherer_tracker{i}(1:3) ...
            -lineGatherer_detector{order(i)}(1:3));
        maxAllowed = [N_expert_op.maxChangeInXf ...
            N_expert_op.maxChangeInAngle N_expert_op.maxChangeInBend];
        notAccurateInImage = sum(double(diffs > maxAllowed));
        if notAccurateInImage
            order(i) = 0;
            disp(['Fit of whisker ' num2str(i) ' is estimated as not accurate by expert.']);
        end
    end
end

missedWhiskers = find(order == 0);
missedClusters = [];
for i = 1:length(lineGatherer_detector)
    if isempty(find(order==i,1))
        missedClusters = [missedClusters i];
    end
end


