function [ distancesq ] = distanceToLine( xf, alpha, b, x_max, dots)
dots = dots - complex(xf);
x_sh = cos(alpha)*real(dots)+sin(alpha)*imag(dots);
y_sh = -sin(alpha)*real(dots)+cos(alpha)*imag(dots);
if x_sh <= x_max
    distancesq = complex((b*x_sh.^2-y_sh).^2);
else
    distancesq = complex(((x_sh-x_max)/20).^2+(y_sh-b*x_max.^2).^2);
end