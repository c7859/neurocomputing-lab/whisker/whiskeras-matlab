
% s = stTidyFilename(s)
%   stTidyFilename() removes spurious slashes and replaces
%   all back slashes with forward slashes, so that the
%   filenames can be functionally compared by string
%   comparison.


%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====


function s = stTidyFilename(s)

% on any platform, we can use forward slashes
s = strrep(s, '\', '/');

% double slashes are always spurious
while ~isempty(strfind(s, '//'))
	s = strrep(s, '//', '/');
end

% and ending with a slash can only cause confusion
while ~isempty(s) && s(end) == '/'
	s = s(1:end-1);
end
