# WhiskEras MATLAB

The original WhiskEras algorithm. License information can be found in the `LICENSE` file.
The algorithm can be run from the `main.m` file. All the necessary parameters can be updated in that file. Note that the code is work in progress: it does contain the full working WhiskEras algorithm, but we are still working on structure, maintainability and user-friendliness.

Files from or based on the BIOTACT Whisker Tracking Tool (BWTT) (<http://bwtt.sourceforge.net>):
* `stExtractWhiskersGrayValuesWithMorphologyAndShave.m`
* `stGenerateUID.m`
* `stGetBackgroundImage.m`
* `stGetUserName.m`
* `stJob.m`
* `stLoadMovieFrames.m`
* `stProcessPath.m`
* `stTidyFilename.m`
* `stUserData.m`


License information can be found on <http://bwtt.sourceforge.net/fairuse/>


Files based on work by Yarpiz (<https://yarpiz.com>):
* `DBSCAN.m`
* `PlotClusterinResult.m`

License information can be found in `Yarpiz_license.txt`

The other files contain original code by J.L.F. Betting, MA MSc.