function [ newDots, newIDX ] = stitching_II( dots, IDX, stitch_II_opt)
%ROOTINGWHISKERS Checking which whiskers are floating and 'inoculate' them
% on the whisker they were hiding behind.
% Use: give variables dots (all the whisker points), IDX (clustered whiskers),
% highestRoot (highest acceptable y-distance from the snout) and the
% acceptable distance between the bottom of a 'floating' whisker and the
% whisker it should be inoculated to.

u = 1;
while u <= max(IDX)                                                     % Small clusters (defined by var minClusterSize)
    isi = (IDX == u);                                                   % are marked as noise, the IDX vector is changed so that there
    if sum(isi) < stitch_II_opt.minClusterSize                                        % are no numbers 1 <= n <= max(IDX) for which sum(IDX == n) is
        IDX = IDX .* ~isi;                                              % equal to zero.
        IDX(IDX > u) = IDX(IDX > u)-1;
    else
        u = u + 1;
    end
end
newDots = dots;                                                             % Prepare output
newIDX = IDX;
for i = 1:max(IDX)                                                          % For all detected whiskers
    if min(dots(IDX==i,2)) > stitch_II_opt.highestRoot                                    % Floating?
        Reb = rotationData(dots(IDX == i,:),stitch_II_opt.lengthOfTipAndBottom,'b');  % For every cluster, determine rotation matrix for the tail, and begin and end points.
        otherClusters = dots(IDX ~= i,:);                                   % Divide current whisker and other whiskers
        IDX_other = IDX(IDX ~= i);
        otherClusters_rotated = [Reb.R*(otherClusters-Reb.beginPoint)']';          % Align other whiskers with the tail of the current whisker
        otherClusters_acc = otherClusters_rotated(abs( ...                  % Find all the dots that would be 'acceptable' as inoculation point.
            otherClusters_rotated(:,2)) < stitch_II_opt.acceptableYdistance ...
            & otherClusters_rotated(:,1) < 0,:);
        if  isempty(otherClusters_acc)
            newDots(newIDX==i,:) = [];
            newIDX(newIDX==i) = [];
            continue;                                                       % If not possible, then don't inoculate.
        end
        IDX_acc = IDX_other(abs(otherClusters_rotated(:,2)) < ...           % Find to which cluster the 'acceptable' dots belong
            stitch_II_opt.acceptableYdistance & otherClusters_rotated(:,1) < 0);
        [xd, I] = min(abs(otherClusters_acc(:,1)));                         % Find the best inoculation point (closest to the tail)
        if xd > stitch_II_opt.acceptableXdistance
            newDots(newIDX==i,:) = [];
            newIDX(newIDX==i) = [];
            continue;
        else
           backRotated =  (Reb.R^-1)*otherClusters_acc(I,:)'+Reb.beginPoint';      % Rotate this point to original state
           chosenCluster = IDX_acc(I);                                      % Check which cluster this was
           addendum = dots((dots(:,2) < backRotated(2)) & (IDX == ...          % and add all the whisker points of this cluster, below the inoculation point, to the floating whisker.
               chosenCluster),:);
           newDots = [newDots; addendum];
           newIDX = [newIDX; ones(size(addendum,1),1)*i];
        end
    end
end


