function [ order, missedClusters, missedWhiskers, fitMissedWhiskers ] = knnFitting(lineGatherer_tracker, lineGatherer_detector, changeVector, knn)
%KNNFITTING Summary of this function goes here
%   Detailed explanation goes here
firstChoice = zeros(1,length(lineGatherer_detector));
secondChoice = zeros(1,length(lineGatherer_detector));
distanceFirstChoice = zeros(1,length(lineGatherer_detector));
distanceSecondChoice = zeros(1,length(lineGatherer_detector));
confidenceFirstChoice = zeros(1,length(lineGatherer_detector));
confidenceSecondChoice = zeros(1,length(lineGatherer_detector));
currentClusters = [];
order = zeros(size(lineGatherer_tracker'));
length_m = zeros(size(lineGatherer_tracker));
length_s = zeros(size(lineGatherer_tracker));
currentMeanXf = mean(cellfun(@(x) x(1), lineGatherer_detector));
currentMeanA  = mean(cellfun(@(x) x(2), lineGatherer_detector));

figure(6)
clf


for i = 1:length(lineGatherer_detector)
    x12 = knn.rotationMatrix*[lineGatherer_detector{i}(1)-currentMeanXf; cot(lineGatherer_detector{i}(2))-cot(currentMeanA)];
    currentClusters = [currentClusters; x12(1,:)' x12(2,:)' lineGatherer_detector{i}(4)];
end


unfoldedWhiskers = unfoldWhiskers(knn.whiskerDB,knn.rotationMatrix);
unfoldedWhiskers(:,1:2) = (unfoldedWhiskers(:,1:2)-knn.meanNormalization)./knn.stdNormalization;
currentClusters(:,1:2) = (currentClusters(:,1:2)-knn.meanNormalization)./knn.stdNormalization;

for i = 1:length(lineGatherer_tracker)
    length_m(i) = mean(unfoldedWhiskers(unfoldedWhiskers(:,4)==i,3));
    length_s(i) = std(unfoldedWhiskers(unfoldedWhiskers(:,4)==i,3));
end
LOfClusters = cellfun(@(x) x(4),lineGatherer_detector);
%% First: detemine first and second choices for every cluster.

for i = 1:length(lineGatherer_detector)
    
    currentPoint = [currentClusters(i,:) 0];
    %     clf
    %     for j = 1:max(unfoldedWhiskers(:,4))
    %         if sum(unfoldedWhiskers(:,4)==j)
    %             scatter(unfoldedWhiskers(unfoldedWhiskers(:,4)==j,1),unfoldedWhiskers(unfoldedWhiskers(:,4)==j,2),'.');
    %             hold on
    %         end
    %     end
    %  scatter(currentPoint(1),currentPoint(2));
    %  hold on
    distances = unfoldedWhiskers - currentPoint;
    x1x2d = [(distances(:,1)*knn.theta1).^2+(distances(:,2)*knn.theta1).^2 distances(:,4)];
    x1x2d = sortrows(x1x2d,1,'ascend');
    if size(x1x2d,1) > knn.k
        x1x2d = x1x2d(1:knn.k,:);
    end
    [firstChoice(i),c] = mode(x1x2d(:,2));
    nrEls = double(size(x1x2d,1));
    confidenceFirstChoice(i) = double(c)/nrEls;
    distanceFirstChoice(i) = mean(x1x2d(x1x2d(:,2)==firstChoice(i),1));
    if confidenceFirstChoice(i) < knn.security
        x1x2d = x1x2d(x1x2d(:,2) ~= firstChoice(i),:);
        [secondChoice(i),c] = mode(x1x2d(:,2));
        confidenceSecondChoice(i) = double(c)/nrEls;
        distanceSecondChoice(i) = mean(x1x2d(x1x2d(:,2)==secondChoice(i),1));
    end
end

%% Remove all the choices that are not allowed from the perspective of max. whisker movement per frame.

for i = 1:length(firstChoice)
    if firstChoice(i)
        diff1 = lineGatherer_detector{i}(1:3)-lineGatherer_tracker{firstChoice(i)}(1:3);
        diff1 = diff1 + [currentMeanXf-knn.previousMeanXf currentMeanA-knn.previousMeanA 0];
        maxDifferences = [knn.maxChangeInXf knn.maxChangeInAngle changeVector.maxChangeInBend];
        if sum(diff1 > maxDifferences)
            firstChoice(i) = 0;
        end
    end
    if secondChoice(i)
        diff2 = abs(lineGatherer_detector{i}(1:3)-lineGatherer_tracker{secondChoice(i)}(1:3));
        if sum(diff2 > maxDifferences)
            secondChoice(i) = 0;
        end
    end
end

for i = 1:length(lineGatherer_tracker)
    [~, aI] = find(firstChoice == i);
    [~, bI] = find(secondChoice == i);
    if isempty(aI)
        continue;
    end
    if length(aI) == 1                                                          % Easiest case: there's one match in 'first choice' with a high confidence
        order(i) = aI;
        firstChoice(aI) = 0;
        secondChoice(aI) = 0;
        secondChoice(bI) = zeros(size(secondChoice(bI)));
        secondChoice(aI) = 0;
    elseif length(aI) > 1                                                       % Multiple clusters fit this line as their first choice.
        confaI = confidenceFirstChoice(aI);
        [c1,cI] = max(confaI);
        if ~isempty(confaI(confaI < c1))
            binaryConfidence = c1/max(confaI(confaI < c1));
        else
            binaryConfidence = 0;
        end
        distaI = distanceFirstChoice(aI);
        [d1,dI] = min(distaI);
        if ~isempty(distaI(distaI > d1))
            binaryDistance = min(distaI(distaI > d1))/d1;
        else
            binaryDistance = 0;
        end
        if binaryConfidence >= knn.confidenceDecisionFactor
            order(i) = aI(cI);
            firstChoice(aI(cI)) = 0;
            secondChoice(aI(cI)) = 0;
            secondChoice(bI) = zeros(size(secondChoice(bI)));
        elseif binaryDistance >= knn.distanceDecisionFactor
            order(i) = aI(dI);
            firstChoice(aI(dI)) = 0;
            secondChoice(aI(dI)) = 0;
            secondChoice(bI) = zeros(size(secondChoice(bI)));
        else
            disp(['WARNING: couldn''t fit whisker ' num2str(i) ' to unique cluster based on position features. Will use length.']);
            chances = zeros(size(aI));
            for j = 1:length(chances)
                chances(j) = normpdf(LOfClusters(aI(j)),length_m(i),length_s(i));
            end
            [~,I] = max(chances);
            order(i) = aI(I);
            firstChoice(aI(I)) = 0;
            secondChoice(aI(I)) = 0;
            secondChoice(bI) = zeros(size(secondChoice(bI)));
        end
    else
        disp(['WARNING: couldn''t fit whisker ' num2str(i) '.']);
    end
end
for i = 1:length(lineGatherer_tracker)
    if order(i)
        continue;
    end
    [~, bI] = find(secondChoice == i);
    if ~isempty(bI)% this means that none of the clusters has this whisker as its first choice, but there are second choices.
        if length(bI) == 1                                                  % Easiest case: there's one match in 'second choice'.
            order(i) = bI;
            secondChoice(bI) = 0;
            firstChoice(bI) = 0;
        else
            confaI = confidenceSecondChoice(bI);
            [c1,cI] = max(confaI);
            if ~isempty(confaI(confaI < c1))
                binaryConfidence = c1/max(confaI(confaI < c1));
            else
                binaryConfidence = 0;
            end
            distaI = distanceSecondChoice(bI);
            [d1,dI] = min(distaI);
            if ~isempty(distaI(distaI > d1))
                binaryDistance = min(distaI(distaI > d1))/d1;
            else
                binaryDistance = 0;
            end
            if binaryConfidence >= knn.confidenceDecisionFactor
                order(i) = bI(cI);
                secondChoice(bI) = zeros(size(secondChoice(bI)));
                firstChoice(bI(cI)) = 0;
            elseif binaryDistance >= knn.distanceDecisionFactor
                order(i) = bI(dI);
                secondChoice(bI) = zeros(size(secondChoice(bI)));
                firstChoice(bI(cI)) = 0;
            else
                disp(['WARNING: couldn''t fit whisker ' num2str(i) ' to unique cluster based on position features. Will use length.']);
                chances = zeros(size(bI));
                for j = 1:length(chances)
                    chances(j) = abs(LOfClusters(bI(j))-length_m(i));
                end
                [~,I] = min(chances);
                order(i) = bI(I);
                secondChoice(bI) = zeros(size(secondChoice(bI)));
            end
        end
    end
end


missedWhiskers = find(order == 0);
missedClusters = [];
for i = 1:length(lineGatherer_detector)
    if ~sum(order == i)
        missedClusters = [missedClusters i];
    end
end
fitMissedWhiskers = lineGatherer_tracker(missedWhiskers);


end

