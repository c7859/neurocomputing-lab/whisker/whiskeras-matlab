function [ nbc ] = determineNeighbor (          r1c1, r1c2, r1c3, r1c4, r1c5, ...
                                                r2c1, r2c2, r2c3, r2c4, r2c5, ...
                                                r3c1, r3c2, r3c3, r3c4, r3c5, ...
                                                r4c1, r4c2, r4c3, r4c4, r4c5, ...
                                                r5c1, r5c2, r5c3, r5c4, r5c5, ...
                                                a_r1c1, a_r1c2, a_r1c3, a_r1c4, a_r1c5, ...
                                                a_r2c1, a_r2c2, a_r2c3, a_r2c4, a_r2c5, ...
                                                a_r3c1, a_r3c2, a_r3c3, a_r3c4, a_r3c5, ...
                                                a_r4c1, a_r4c2, a_r4c3, a_r4c4, a_r4c5, ...
                                                a_r5c1, a_r5c2, a_r5c3, a_r5c4, a_r5c5, chosenSide, wad)
%DETERMINENEIGHBOR Stencil operation for adapted version of Steger's local
%clustering algorith
%   The middle pixel (r3c3 and a_r3c3) peeks in its neighborhood and
%   connects to at most one other pixel, for which
%   |[distance]}+wad*|[angle difference]| is minimized. The side is defined by the
%   flag set in <chosenSide>, the region where the pixel peeks by its local
%   direction. The code is repetitive and basic, because this function was designed
%   to be executed on a GPU.

if ~imag(a_r3c3)
    nbc = 0;
else
    dist = Inf;
    nbc = 0;
        if (abs(real(a_r3c3)) < 22.5/180*pi)
            if chosenSide
                %% [0 1 1 1 0]
                %  [0 1 1 1 0]
                %  [0 0 0 0 0]
                %  [0 0 0 0 0]
                %  [0 0 0 0 0]
                dist_local = abs(r1c2-r3c3)+wad*abs(real(a_r3c3)-real(a_r1c2));
                if  dist_local < dist
                    nbc = imag(a_r1c2);
                    dist = dist_local;
                end
                dist_local = abs(r1c3-r3c3)+wad*abs(real(a_r3c3)-real(a_r1c3));
                if  dist_local < dist
                    nbc = imag(a_r1c3);
                    dist = dist_local;
                end                
                dist_local = abs(r1c4-r3c3)+wad*abs(real(a_r3c3)-real(a_r1c4));
                if  dist_local < dist
                    nbc = imag(a_r1c4);
                    dist = dist_local;
                end                   
                dist_local = abs(r2c2-r3c3)+wad*abs(real(a_r3c3)-real(a_r2c2));
                if  dist_local < dist
                    nbc = imag(a_r2c2);
                    dist = dist_local;
                end
                dist_local = abs(r2c3-r3c3)+wad*abs(real(a_r3c3)-real(a_r2c3));
                if  dist_local < dist
                    nbc = imag(a_r2c3);
                    dist = dist_local;
                end                
                dist_local = abs(r2c4-r3c3)+wad*abs(real(a_r3c3)-real(a_r2c4));
                if  dist_local < dist
                    nbc = imag(a_r2c4);
                end
            else
                %% [0 0 0 0 0]
                %  [0 0 0 0 0]
                %  [0 0 0 0 0]
                %  [0 1 1 1 0]
                %  [0 1 1 1 0]               
                dist_local = abs(r4c2-r3c3)+wad*abs(real(a_r3c3)-real(a_r4c2));
                if  dist_local < dist
                    nbc = imag(a_r4c2);
                    dist = dist_local;
                end
                dist_local = abs(r4c3-r3c3)+wad*abs(real(a_r3c3)-real(a_r4c3));
                if  dist_local < dist
                    nbc = imag(a_r4c3);
                    dist = dist_local;
                end                
                dist_local = abs(r4c4-r3c3)+wad*abs(real(a_r3c3)-real(a_r4c4));
                if  dist_local < dist
                    nbc = imag(a_r4c4);
                    dist = dist_local;
                end                   
                dist_local = abs(r5c2-r3c3)+wad*abs(real(a_r3c3)-real(a_r5c2));
                if  dist_local < dist
                    nbc = imag(a_r5c2);
                    dist = dist_local;
                end
                dist_local = abs(r5c3-r3c3)+wad*abs(real(a_r3c3)-real(a_r5c3));
                if  dist_local < dist
                    nbc = imag(a_r5c3);
                    dist = dist_local;
                end                
                dist_local = abs(r5c4-r3c3)+wad*abs(real(a_r3c3)-real(a_r5c4));
                if  dist_local < dist
                    nbc = imag(a_r5c4);
                end
            end
        elseif (real(a_r3c3) >= -67.5/180*pi) && (real(a_r3c3) < -22.5/180*pi)
            if chosenSide
                %% [0 0 0 1 1]
                %  [0 0 1 1 1]
                %  [0 0 0 1 0]
                %  [0 0 0 0 0]
                %  [0 0 0 0 0]
                dist_local = abs(r1c4-r3c3)+wad*abs(real(a_r3c3)-real(a_r1c4));
                if  dist_local < dist
                    nbc = imag(a_r1c4);
                    dist = dist_local;
                end                
                dist_local = abs(r1c5-r3c3)+wad*abs(real(a_r3c3)-real(a_r1c5));
                if  dist_local < dist
                    nbc = imag(a_r1c5);
                    dist = dist_local;
                end                
                dist_local = abs(r2c3-r3c3)+wad*abs(real(a_r3c3)-real(a_r2c3));
                if  dist_local < dist
                    nbc = imag(a_r2c3);
                    dist = dist_local;
                end 
                dist_local = abs(r2c4-r3c3)+wad*abs(real(a_r3c3)-real(a_r2c4));
                if  dist_local < dist
                    nbc = imag(a_r2c4);
                    dist = dist_local;
                end   
                dist_local = abs(r2c5-r3c3)+wad*abs(real(a_r3c3)-real(a_r2c5));
                if  dist_local < dist
                    nbc = imag(a_r2c5);
                    dist = dist_local;
                end   
                dist_local = abs(r3c4-r3c3)+wad*abs(real(a_r3c3)-real(a_r3c4));
                if  dist_local < dist
                    nbc = imag(a_r3c4);
                end
            else
                %% [0 0 0 0 0]
                %  [0 0 0 0 0]
                %  [0 1 0 0 0]
                %  [1 1 1 0 0]
                %  [1 1 0 0 0]
                dist_local = abs(r3c2-r3c3)+wad*abs(real(a_r3c3)-real(a_r3c2));
                if  dist_local < dist
                    nbc = imag(a_r3c2);
                    dist = dist_local;
                end
                dist_local = abs(r4c1-r3c3)+wad*abs(real(a_r3c3)-real(a_r4c1));
                if  dist_local < dist
                    nbc = imag(a_r4c1);
                    dist = dist_local;
                end     
                dist_local = abs(r4c2-r3c3)+wad*abs(real(a_r3c3)-real(a_r4c2));
                if  dist_local < dist
                    nbc = imag(a_r4c2);
                    dist = dist_local;
                end
                dist_local = abs(r4c3-r3c3)+wad*abs(real(a_r3c3)-real(a_r4c3));
                if  dist_local < dist
                    nbc = imag(a_r4c3);
                    dist = dist_local;
                end
                dist_local = abs(r5c1-r3c3)+wad*abs(real(a_r3c3)-real(a_r5c1));
                if  dist_local < dist
                    nbc = imag(a_r5c1);
                    dist = dist_local;
                end
                dist_local = abs(r5c2-r3c3)+wad*abs(real(a_r3c3)-real(a_r5c2));
                if  dist_local < dist
                    nbc = imag(a_r5c2);
                end
            end
        elseif (real(a_r3c3) <= 67.5/180*pi) && (real(a_r3c3) > 22.5/180*pi)
            if chosenSide
                %% [1 1 0 0 0]
                %  [1 1 1 0 0]
                %  [0 1 0 0 0]
                %  [0 0 0 0 0]
                %  [0 0 0 0 0]
                dist_local = abs(r1c1-r3c3)+wad*abs(real(a_r3c3)-real(a_r1c1));
                if  dist_local < dist
                    nbc = imag(a_r1c1);
                    dist = dist_local;
                end                
                dist_local = abs(r1c2-r3c3)+wad*abs(real(a_r3c3)-real(a_r1c2));
                if  dist_local < dist
                    nbc = imag(a_r1c2);
                    dist = dist_local;
                end                
                dist_local = abs(r2c1-r3c3)+wad*abs(real(a_r3c3)-real(a_r2c1));
                if  dist_local < dist
                    nbc = imag(a_r2c1);
                    dist = dist_local;
                end 
                dist_local = abs(r2c2-r3c3)+wad*abs(real(a_r3c3)-real(a_r2c2));
                if  dist_local < dist
                    nbc = imag(a_r2c2);
                    dist = dist_local;
                end   
                dist_local = abs(r2c3-r3c3)+wad*abs(real(a_r3c3)-real(a_r2c3));
                if  dist_local < dist
                    nbc = imag(a_r2c3);
                    dist = dist_local;
                end   
                dist_local = abs(r3c2-r3c3)+wad*abs(real(a_r3c3)-real(a_r3c2));
                if  dist_local < dist
                    nbc = imag(a_r3c2);
                end
            else
                %% [0 0 0 0 0]
                %  [0 0 0 0 0]
                %  [0 0 0 1 0]
                %  [0 0 1 1 1]
                %  [0 0 0 1 1]
                dist_local = abs(r3c4-r3c3)+wad*abs(real(a_r3c3)-real(a_r3c4));
                if  dist_local < dist
                    nbc = imag(a_r3c4);
                    dist = dist_local;
                end
                dist_local = abs(r4c3-r3c3)+wad*abs(real(a_r3c3)-real(a_r4c3));
                if  dist_local < dist
                    nbc = imag(a_r4c3);
                    dist = dist_local;
                end     
                dist_local = abs(r4c4-r3c3)+wad*abs(real(a_r3c3)-real(a_r4c4));
                if  dist_local < dist
                    nbc = imag(a_r4c4);
                    dist = dist_local;
                end
                dist_local = abs(r4c5-r3c3)+wad*abs(real(a_r3c3)-real(a_r4c5));
                if  dist_local < dist
                    nbc = imag(a_r4c5);
                    dist = dist_local;
                end
                dist_local = abs(r5c4-r3c3)+wad*abs(real(a_r3c3)-real(a_r5c4));
                if  dist_local < dist
                    nbc = imag(a_r5c4);
                    dist = dist_local;
                end
                dist_local = abs(r5c5-r3c3)+wad*abs(real(a_r3c3)-real(a_r5c5));
                if  dist_local < dist
                    nbc = imag(a_r5c5);
                end
            end
        else
            if chosenSide
                %% [0 0 0 0 0]
                %  [1 1 0 0 0]
                %  [1 1 0 0 0]
                %  [1 1 0 0 0]
                %  [0 0 0 0 0]
                dist_local = abs(r2c1-r3c3)+wad*abs(real(a_r3c3)-real(a_r2c1));
                if  dist_local < dist
                    nbc = imag(a_r2c1);
                    dist = dist_local;
                end                
                dist_local = abs(r2c2-r3c3)+wad*abs(real(a_r3c3)-real(a_r2c2));
                if  dist_local < dist
                    nbc = imag(a_r2c2);
                    dist = dist_local;
                end                
                dist_local = abs(r3c1-r3c3)+wad*abs(real(a_r3c3)-real(a_r3c1));
                if  dist_local < dist
                    nbc = imag(a_r3c1);
                    dist = dist_local;
                end 
                dist_local = abs(r3c2-r3c3)+wad*abs(real(a_r3c3)-real(a_r3c2));
                if  dist_local < dist
                    nbc = imag(a_r3c2);
                    dist = dist_local;
                end   
                dist_local = abs(r4c1-r3c3)+wad*abs(real(a_r3c3)-real(a_r4c1));
                if  dist_local < dist
                    nbc = imag(a_r4c1);
                    dist = dist_local;
                end   
                dist_local = abs(r4c2-r3c3)+wad*abs(real(a_r3c3)-real(a_r4c2));
                if  dist_local < dist
                    nbc = imag(a_r4c2);
                end
            else
                %% [0 0 0 0 0]
                %  [0 0 0 1 1]
                %  [0 0 0 1 1]
                %  [0 0 0 1 1]
                %  [0 0 0 0 0]
                dist_local = abs(r2c4-r3c3)+wad*abs(real(a_r3c3)-real(a_r2c4));
                if  dist_local < dist
                    nbc = imag(a_r2c4);
                    dist = dist_local;
                end
                dist_local = abs(r2c5-r3c3)+wad*abs(real(a_r3c3)-real(a_r2c5));
                if  dist_local < dist
                    nbc = imag(a_r2c5);
                    dist = dist_local;
                end     
                dist_local = abs(r3c4-r3c3)+wad*abs(real(a_r3c3)-real(a_r3c4));
                if  dist_local < dist
                    nbc = imag(a_r3c4);
                    dist = dist_local;
                end
                dist_local = abs(r3c5-r3c3)+wad*abs(real(a_r3c3)-real(a_r3c5));
                if  dist_local < dist
                    nbc = imag(a_r3c5);
                    dist = dist_local;
                end
                dist_local = abs(r4c4-r3c3)+wad*abs(real(a_r3c3)-real(a_r4c4));
                if  dist_local < dist
                    nbc = imag(a_r4c4);
                    dist = dist_local;
                end
                dist_local = abs(r4c5-r3c3)+wad*abs(real(a_r3c3)-real(a_r4c5));
                if  dist_local < dist
                    nbc = imag(a_r4c5);
                end
            end
        end
end
end

