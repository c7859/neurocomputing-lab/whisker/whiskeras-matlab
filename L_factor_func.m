function [ output_args ] = L_factor_func( input_args )
%L_FACTOR_FUNC This function is meant to be executed on a GPU.
%   Will take the input. If it is smaller than 1, it will return the
%   inverse, otherwise it will return the input.
if input_args < 1
    output_args = 1/input_args;
else
    output_args = input_args;
end
end

