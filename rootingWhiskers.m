function [ newDots, newIDX ] = rootingWhiskers( dots, IDX, highestRoot, acceptableYdistance, acceptableXdistance)
%ROOTINGWHISKERS Checking which whiskers are floating and 'inoculate' them
% on the whisker they were hiding behind.
% Use: give variables dots (all the whisker points), IDX (clustered whiskers),
% highestRoot (highest acceptable y-distance from the snout) and the
% acceptable distance between the bottom of a 'floating' whisker and the
% whisker it should be inoculated to.

newDots = dots;                                                             % Prepare output
newIDX = IDX;

for i = 1:max(IDX)                                                          % For all detected whiskers
    if min(dots(IDX==i,2)) > highestRoot                                    % Floating?
        Reb = R_end_begin(dots(IDX == i,:),1);                              % For every cluster, determine rotation matrix for the tail, and begin and end points.
        otherClusters = dots(IDX ~= i,:);                                   % Divide current whisker and other whiskers
        IDX_other = IDX(IDX ~= i);
        otherClusters_rotated = [Reb{1}*(otherClusters-Reb{3})']';          % Align other whiskers with the tail of the current whisker
        otherClusters_acc = otherClusters_rotated(abs( ...                  % Find all the dots that would be 'acceptable' as inoculation point.
            otherClusters_rotated(:,2)) < acceptableYdistance ...
            & otherClusters_rotated(:,1) < 0,:);
        if  isempty(otherClusters_acc)
            continue;                                                       % If not possible, then don't inoculate.
        end
        IDX_acc = IDX_other(abs(otherClusters_rotated(:,2)) < ...           % Find to which cluster the 'acceptable' dots belong
            acceptableYdistance & otherClusters_rotated(:,1) < 0);
        [xd, I] = min(abs(otherClusters_acc(:,1)));                         % Find the best inoculation point (closest to the tail)
        if xd > acceptableXdistance
            continue;
        else
           backRotated =  (Reb{1}^-1)*otherClusters_acc(I,:)'+Reb{3}';      % Rotate this point to original state
           chosenCluster = IDX_acc(I);                                      % Check which cluster this was
           addendum = dots(dots(:,2) < backRotated(2) & IDX == ...          % and add all the whisker points of this cluster, below the inoculation point, to the floating whisker.
               chosenCluster,:);
           newDots = [newDots; addendum];
           newIDX = [newIDX; ones(size(addendum,1),1)*i];
        end
    end
end

