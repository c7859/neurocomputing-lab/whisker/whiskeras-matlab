function [ order, missedClusters, missedWhiskers] = fitWhiskers2clusters( dots,IDX,lineGatherer,lineGatherer_detector,opts)
%FITWHISKERS2CLUSTERS Input: clusters, preferably clusted well, so that
%every cluster is a whisker. The function will match whisker parametrizations from frame n-1 (defined by
%position on the snout xf, angle to the snout a, bending parameter b and
%length L) to clusters in frame
%   Input:
%   dots, IDX: coordinates and clustering
%   lineGatherer: curves from previous frame
%   changeVector: maximum allowed changes in parameters of previous curves
%   weightOfLengthDifference:

distanceMat = zeros(max(IDX),length(lineGatherer));
lowestDistanceMat = distanceMat;
occMat = zeros(max(IDX),length(lineGatherer));

if size(dots,2) == 2                                                        % Change whisker centerline points representation from
    dots = dots(:,1) + dots(:,2)*sqrt(-1);                                  % [x y] to x + yi
end
dots_g = gpuArray(dots);                                                    % Move all the points to the GPU
lines = cell2mat(lineGatherer');                                            % expand cell array of to matrix, to extract parameters
xf = gpuArray(lines(:,1)');                                                 % and put 4 parameters that define
alpha = gpuArray(lines(:,2)');                                              % a whisker in four separate GPU-arrays
b = gpuArray(lines(:,3)');
x_maxes = gpuArray(lines(:,5)');                                            % We're not taking the actual length, but the position of tip of the whisker on the axis that crosses X with angle alpha

order = zeros(length(lineGatherer),1);                                      % Prepare vector for ordering
distances = real(arrayfun(@(xf, alpha, b, x_maxes, d) ...
    distanceToLine( xf, alpha, b, x_maxes, d ), xf, alpha, b, ...
    x_maxes, dots_g));                                                      % For every whisker point, calculate the mean square distance to every parametrization
for i = 1:max(IDX)                                                          % Now take every cluster
    distanceMat(i,:) = gather(mean(distances(IDX == i,:)));                 % Calculate mean distances of cluster to each parametrization
    lowestDistanceMat(i,:) = gather(min(abs(distances(IDX==i,:))));         % Calculate smallest distance between a point and the parametriatoin           
    distanceMat(i,:) = distanceMat(i,:) + double(distanceMat(i,:) > ...     % If the mean square distance is too high...
        opts.maxMatchDistance)*100000;
    distanceMat(i,:) = distanceMat(i,:) + ...                               % ... or the point that is closest to the parametrization is still too far away.
        double(lowestDistanceMat(i,:) > ...
        opts.maxLowestDistance)*100000;                                     % we're not matching the parametrization with the cluster.
end
while 1                                                                     % Matching clusters and parametrizations based on their scores
    [minDist, I] = minmat(distanceMat);                                     % Rest is very similar to what was done in `kalmanFitting.m', line 86 and further
    if minDist > 99999
        break;
    end
    distanceMat(I(1),I(2)) = Inf;
    if ~(sum(occMat(I(1),:)) || sum(occMat(:,I(2))))
        occMat(I(1),I(2)) = 1;
        order(I(2)) = I(1);
    end
    if prod(sum(occMat,1)) || prod(sum(occMat,2))
        break;
    end
end
missedClusters = [];
for i = 1:max(IDX)
    if ~sum(order==i)
        missedClusters = [missedClusters i];
    end
end
missedWhiskers = find(order == 0);
end

