function [ value, indices ] = minmat( input_args )
%MINMAT Small function that gets the minimum value of a 2D matrix, and its
%incides. Normal min() function in MATLAB only gets the right index in the
%case of a vector, not with a matrix, so that's why we wrote this function.
sz = size(input_args);
[value, index] = min(input_args(:));
column = floor((index-1)/sz(1))+1;
row = index-(column-1)*sz(1);
indices = [row column];

end

