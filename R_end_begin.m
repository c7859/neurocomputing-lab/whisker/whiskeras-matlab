function [output_values] = R_end_begin( dots, flag )
%COV_AVG Give a cluster and a flag, and it will return a rotation matrix,
%the begin point, the end point, and the length as cells.
%   Use of 'flag':
%   0: rotation matrix aligns with cluster bottom
%   1: rotation matrix aligns with cluster top
%   2: rotation matrix aligns with average of whisker (i.e. eigenvector if
%   largest eigenvalue;
covMat = cov(dots);                                                         % Determine covariance matrix of cluster
[eigenvectors,eigenvalues] = eig(covMat);                                   % And determine its eigenvalues and eigenvectors
eigenvalues = sum(eigenvalues);
[~,I] = max(abs(eigenvalues));                                              % Find largest eigenvalue
ev = eigenvectors(:,I);                                                     % Take the corresponding eigenvector
if ev(2) < 0                                                                
    ev = -ev;                                                               % Eigenvector should always point away from the snout.
end
R_init = [ev(1) ev(2); -ev(2) ev(1)];                                       % Construct rotation matrix
rotatedDots = [R_init*dots'];                                               % Rotate all the cluster
[~,I] = max(rotatedDots(1,:));                                              % Find its top
[~,J] = min(rotatedDots(1,:));                                              % and bottom
endPoint = dots(I,:);                                                       % coordinates
beginPoint = dots(J,:);
L = sqrt(sum((endPoint-beginPoint).^2));                                    % Determine the approximate length of the cluster (whisker)
if flag==1                                                                  % Decide whether to align to the top or bottom.
    rotatedDots = [R_init*[dots-beginPoint]']';
    selectedDots = dots(rotatedDots(:,1) < 30,:);
elseif flag==0
    rotatedDots = [R_init*[dots-endPoint]']';
    selectedDots = dots(rotatedDots(:,1) > -30,:);
end
if flag < 2                                                                 % Repeat procedure, now just with the 40 highest or lowest points (the 'head' or 'tail' of the cluster);
    if size(selectedDots,1) < 2
        output_values = 0;
    else
        covMat = cov(selectedDots);
        [eigenvectors,eigenvalues] = eig(covMat);
        eigenvalues = sum(eigenvalues);
        [~,I] = max(eigenvalues);
        ev = eigenvectors(:,I);
        if ev(2) < 0
            ev = -ev;
        end
        R = [ev(1) ev(2); -ev(2) ev(1)];
        output_values = {R, endPoint, beginPoint, L};
    end
else
    output_values = {R_init, endPoint, beginPoint, L};                      % Or return the general rotation matrix, together with the begin- and endpoint and length. 
end

end

